package com.zuitt.s01a1;

public class ActivityDataTypesAndVariables {
    public static void main (String[] args) {
        //Create a variable for the first name.
        //Create a variable for the last name.
        //Create a variable for the grade in English.
        //Create a variable for the grade in Mathematics.
        //Create a variable for the grade in Science.
        //Compute the average grade of the three subjects.
        //Display a message that states the user’s complete name and the average grade of the 3 subjects.

        String firstName = "Jovelyn";
        String lastName = "Mendez";
        double gradeEnglish = 94.50;
        double gradeMathematics = 97.75;
        double gradeScience = 95.25;
        double averageGrade = 0;
        averageGrade = (gradeEnglish + gradeMathematics + gradeScience) / 3;

        System.out.println("Full name : " + firstName + " " + lastName);
        System.out.println("Average grade is: " + averageGrade);

    }
}
